
build: _build
	tsc --module amd js/app.ts --outDir build/js
	cp -r js/lib/*.min.js ./build/js/lib/
	cp app.css index.html build/

.PHONY: build

_build:
	rm -rf build
	mkdir build
	mkdir build/js/
	mkdir build/js/lib

clean:
	rm -f js/*.js
	rm -f js/*.map.js
	rm -rf build

stable:
	git tag -d stable
	git push origin :refs/tags/stable
	git tag stable
	git push --tags
