﻿/// <reference path="./lib/require.d.ts" />
/// <reference path="./lib/jquery.d.ts" />

import Init = require("init");

function enableRun() {
    $("#run-button").css({ display: "block" });
}

function disableRun() {
    $("#run-button").css({ display: "none" });
}

function enableSave() {
    $("#save-button").css({ display: "block" });
}

function disableSave() {
    $("#save-button").css({ display: "none" });
}

function enableLoad() {
    $("#loader-button").css({ display: "block" });
}

function disableLoad() {
    $("#loader-button").css({ display: "none" });
}

function enableAll() {
    enableLoad();
    enableRun();
    enableSave();
}

function disableAll() {
    disableLoad();
    disableRun();
    disableSave();
}

function performClick(node) {
    var evt = document.createEvent("MouseEvents");
    evt.initEvent("click", true, true);
    node.dispatchEvent(evt);
}



var MAX_TEX_SIZE = 1600;


(function () {
    enableLoad();
    disableRun();
    disableSave();

    var image;
    var finalImage;
    var currentFilename;
    var imageWidth = 0;
    var imageHeight = 0;
    var patternWidth = 20;
    var patternHeight = 20;
    var i = 0;

    $("#loader-button").on("click", function () {
        performClick($("#file-browser").get(0));
    });


    // when the user has selected a new image
    $("#file-browser").get(0).addEventListener("change", function (event: any) {
        $("#source-image").remove();
        $("#generated-image").remove();

        var file: File = event.target.files[0];

        currentFilename = file.name;
        var reader = new FileReader();

        reader.onload = function (event: any) {
            image = new Image();
            image.onload = function () {
                var previewImage = new Image();
                previewImage.src = image.src;
                previewImage.onload = function () {
                    previewImage.setAttribute("id", "source-image");
                    $("#container").prepend(previewImage);
                    enableRun();
                }
            }

            image.src = event.target.result;
        }

        reader.readAsDataURL(file);
    }, false);


    // run shader!
    $("#run-button").on("click", function () {
        disableAll();
        $("#generated-image").remove();

        setTimeout(function () {
            var targetWidth = $("#source-image").width();
            var targetHeight = $("#source-image").height();
            if (image.width > MAX_TEX_SIZE || image.height > MAX_TEX_SIZE) {
                if (image.width > image.height) {
                    targetWidth = MAX_TEX_SIZE;
                    targetHeight = Math.floor(MAX_TEX_SIZE * image.height / image.width);
                } else {
                    targetWidth = Math.floor(MAX_TEX_SIZE * image.width / image.height);
                    targetHeight = MAX_TEX_SIZE;
                }
            }
            finalImage = Init.renderImage(image, patternWidth, patternHeight, targetWidth, targetHeight);
            var $final = $(finalImage);

            $("#container").append(finalImage);
            $(finalImage).attr("id", "generated-image");
            enableAll();
        }, 32);
    });


    $("#element-width").on("change", function () {
        $("#width-text").text("# element on width = " + this.value);
        patternWidth = parseInt(this.value);
    });

    $("#element-width").on("mousemove", function () {
        $("#width-text").text("# element on width = " + this.value);
        patternWidth = parseInt(this.value);
    });

    $("#element-height").on("change", function () {
        $("#height-text").text("# element on height = " + this.value);
        patternHeight = parseInt(this.value);
    });

    $("#element-height").on("mousemove", function () {
        $("#height-text").text("# element on height = " + this.value);
        patternHeight = parseInt(this.value);
    });

    $("#save-button").on("click", function () {

        var save = <any> document.createElement('a');
        save.href = finalImage.src;
        save.target = '_blank';
        save.download = "crys-" + currentFilename;


        performClick(save);
        var win: any = window;
        (win.URL || win.webkitURL).revokeObjectURL(save.href);
       // window.location.href = image.src.replace('image/png', 'image/octet-stream');
    });
})();
