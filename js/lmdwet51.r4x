﻿import HeightMap = require("height-map");

export class Triangulizer {
    public patternTexture: THREE.RenderTarget;
    public colorLocationTexture: THREE.RenderTarget;
    private colorBuffer: THREE.Texture;

    private patternScene: THREE.Scene;
    private colorLocationScene: THREE.Scene;
    private patternParameters: THREE.ShaderMaterialParameters;

    public colorComputerTexture: THREE.RenderTarget;
    private colorComputerGeometry: THREE.BufferGeometry;
    private colorComputerMaterial: THREE.Material;
    private colorComputerScene: THREE.Scene = new THREE.Scene();

    private patternPosition:  number[] = [];
    private patternId: number[] = [];
    private colorPosition: number[] = [];

    public finalPassRenderTarget: THREE.RenderTarget;
    private finalPassScene: THREE.Scene = new THREE.Scene();
    private finalPassGeometry: THREE.BufferGeometry;
    private finalPassMesh: THREE.Mesh;
    private finalPassMaterial: THREE.ShaderMaterial;

    private elementWidth: number;
    private elementHeight: number;

    private patternWidth = 2.0;
    private patternHeight = 2.0;

    constructor(private width: number, private height: number) {
    }


    private getPatternMaterial() {
        var vertShader = document.getElementById('pattern-vertex').innerHTML;
        var fragShader = document.getElementById('pattern-fragment').innerHTML;

        this.patternParameters = {
            vertexShader: vertShader,
            fragmentShader: fragShader,
            vertexColors: THREE.VertexColors,
            useColor: true
        }


        var material = new THREE.ShaderMaterial(this.patternParameters);

        return material;
    }

    private getColorLocationMaterial() {
        var vertShader = document.getElementById('color-location-vertex').innerHTML;
        var fragShader = document.getElementById('color-location-fragment').innerHTML;

        this.patternParameters = {
            vertexShader: vertShader,
            fragmentShader: fragShader,
            vertexColors: THREE.VertexColors,
            useColor: false,
            attributes: {
                // null = handled in geometry attribute
                aColorLocation: { type: "v2", value: null }
            },
        }


        var material = new THREE.ShaderMaterial(this.patternParameters);

        return material;
    }

    private getFinalPassMaterial() {
        var vertShader = document.getElementById('final-pass-vertex').innerHTML;
        var fragShader = document.getElementById('final-pass-fragment').innerHTML;

        this.patternParameters = {
            uniforms: {
                texColor: { type: "t", value: this.colorComputerTexture },
                texColorLocation: { type: "t", value: this.colorLocationTexture }
            },

            attributes: {
                // null = handled in geometry attribute
                uv: { type: "v2", value: null },
            },
            vertexShader: vertShader,
            fragmentShader: fragShader,
        }

        var material = new THREE.ShaderMaterial(this.patternParameters);

        return material;
    }

    private getColorComputerMaterial(colorBuffer: THREE.RenderTarget) {
        var vertShader = document.getElementById('color-computer-vertex').innerHTML;
        var fragShader = document.getElementById('color-computer-fragment').innerHTML;

        this.patternParameters = {
            uniforms: {
                colorBuffer: { type: "t", value: colorBuffer },
                pixelId: { type: "t", value: this.patternTexture },

                xOffset: { type: "f", value: 1 / this.elementWidth },
                yOffset: { type: "f", value: 1 / this.elementHeight },
            },
            attributes: {
                patternId: { type: "v3", value: null },
                patternPosition: { type: "v2", value: null }
            },
            vertexShader: vertShader,
            fragmentShader: fragShader,
            vertexColors: THREE.VertexColors
        }   

        var material = new THREE.ShaderMaterial(this.patternParameters);

        return material;
    }

    private initPatternGeometry(elementWidth: number, elementHeight: number) {

        var vertices: number[] = [];
        var indices: number[] = [];

        var yOffset = this.patternHeight / elementHeight;
        var xOffset = this.patternWidth / elementWidth;

        var index = 0;

        var startOffset = -xOffset;
        
        // we do one more to fill the left teeth
        for (var i = 0; i < elementHeight; i++) {
            var y0 = this.patternHeight - (i * yOffset);
            var y1 = this.patternHeight - ((i + 1) * yOffset);
            for (var j = 0; j < elementWidth + 1; j++) {
                var x0 = startOffset + ((i & 1) == 0 ? j * xOffset : (j + 0.5) * xOffset);
                var x1 = x0 + xOffset;
                var x2 = x0 + 0.5 * xOffset;
                var x3 = x2 + xOffset;

                var p0 = new THREE.Vector3(x0, y0, 0);
                var p1 = new THREE.Vector3(x1, y0, 0);
                var p2 = new THREE.Vector3(x2, y1, 0);
                var p3 = new THREE.Vector3(x3, y1, 0);


                /*
                Pattern:
                         ____________________________
                        /\1 /\3 /\5 /  ...     /\  /\
                       /0 \/2 \/4 \/   ...    /  \/  \
                */

                // fill vertices
                vertices.push(p0.x, p0.y, p0.z,
                              p2.x, p2.y, p2.z,
                              p1.x, p1.y, p1.z);
                vertices.push(p2.x, p2.y, p2.z,
                              p3.x, p3.y, p3.z,
                              p1.x, p1.y, p1.z);

                // fill indices
                indices.push(index + 0, index + 1, index + 2,
                             index + 3, index + 4, index + 5);

                index += 6;
            }
        }

        var geometry = new THREE.BufferGeometry;
        geometry.addAttribute("index", new Uint16Array(indices), 1);
        geometry.addAttribute("position", new Float32Array(vertices), 3);

        return geometry;
    }

    // we create the pattern
    public initPattern(colorBuffer: THREE.RenderTarget, renderer: THREE.WebGLRenderer, camera: THREE.Camera,
        elementWidth: number, elementHeight: number, canvasWidth: number, canvasHeight: number) {


        this.elementWidth = elementWidth;
        this.elementHeight = elementHeight;

        this.patternTexture = new THREE.WebGLRenderTarget(canvasWidth, canvasHeight,
            {
                minFilter: THREE.LinearFilter, magFilter: THREE.LinearFilter,
                format: THREE.RGBFormat
            });

        this.colorLocationTexture = new THREE.WebGLRenderTarget(canvasWidth, canvasHeight,
            {
                minFilter: THREE.LinearFilter, magFilter: THREE.LinearFilter,
                format: THREE.RGBFormat
            });


        this.colorComputerTexture = new THREE.WebGLRenderTarget(2 * this.elementWidth, this.elementHeight,
            {
                minFilter: THREE.NearestFilter, magFilter: THREE.NearestFilter,
                format: THREE.RGBFormat
            });


        this.finalPassRenderTarget = new THREE.WebGLRenderTarget(canvasWidth, canvasHeight,
            {
                minFilter: THREE.NearestFilter, magFilter: THREE.NearestFilter,
                format: THREE.RGBFormat
            });


        var patternGeometry = this.initPatternGeometry(this.elementWidth, this.elementHeight);

        var r = 0;
        var g = 0;
        var b = 0;
        
        var vertices = patternGeometry.getAttribute("position").array;

        var colorComputationPostion: number[] = [];
        var colorLocation: number[] = [];

        var u = 0;
        var v = 0;
        var count = 0;
        var id1 = 3;
        var id2 = 5;
        var id3 = 7;

        var colors = [];

        for (var i = 0; i < vertices.length; i += 9) {
            // create random RGB ids
            r = (id1 % 256) / 255;
            g = (id2 % 256) / 255;
            b = (id3 % 256) / 255;



            // get position where to start color computation
            var x = Math.min(Math.min(vertices[i], vertices[i + 3]), vertices[i + 6]);
            var y = Math.max(Math.max(vertices[i + 1], vertices[i + 4]), vertices[i + 7]);

            // position in the range [0 - 1] where to write the sum of the pixels for the pattern
            var xOffset = count % (2 * this.elementWidth);
            var yOffset = Math.floor(count / (2 * this.elementWidth));

            xOffset += 0.5;
            xOffset /= (2 * this.elementWidth);
            yOffset += 0.5;
            yOffset = 1 - yOffset / this.elementHeight;
                
            // colorLocation will store the location of where to get the computed color.
            // 
            // For the final phase we render a simple quad that will read the color location
            // and use it to pick up the final color in the computed color texture
            colorLocation.push(xOffset, yOffset, xOffset, yOffset, xOffset, yOffset); 


            // color is used to set the ID of the triangle
            colors.push(r, g, b,
                        r, g, b,
                        r, g, b);

            colorComputationPostion.push(2 * xOffset - 1, 2 * yOffset - 1, 0.0);


            // pattern position defines the top left location where to start the sum
            // we use a point primitve for color computation, therefore we need fill only one point
            this.patternPosition.push(x / this.patternWidth, y / this.patternHeight); 

            this.patternId.push(r, g, b); 


            id1 += 3;
            id2 += 5;
            id3 += 7;
            count++;
        }

        patternGeometry.addAttribute("color", new Float32Array(colors), 3);
        patternGeometry.addAttribute("aColorLocation", new Float32Array(colorLocation), 2);


        // RENDER PATTERN ID TEXTURE
        var patternIdMesh = new THREE.Mesh(patternGeometry, this.getPatternMaterial());
        this.patternScene = new THREE.Scene();
        this.patternScene.add(patternIdMesh);

        renderer.setRenderTarget(this.patternTexture);
        renderer.clear();
        renderer.render(this.patternScene, camera, this.patternTexture); 



        // RENDER COLOR LOCATION TEXTURE
        var patternLocationMesh = new THREE.Mesh(patternGeometry, this.getColorLocationMaterial());
        this.colorLocationScene = new THREE.Scene();
        this.colorLocationScene.add(patternLocationMesh);

        renderer.setRenderTarget(this.colorLocationTexture);
        renderer.clear();
        renderer.render(this.colorLocationScene, camera, this.colorLocationTexture);
        renderer.setRenderTarget(null);


        // COLOR COMPUTER
        this.colorComputerGeometry = new THREE.BufferGeometry();
        this.colorComputerGeometry.addAttribute("patternPosition", new Float32Array(this.patternPosition), 2);
        this.colorComputerGeometry.addAttribute("patternId", new Float32Array(this.patternId), 3);
        this.colorComputerGeometry.addAttribute("position", new Float32Array(colorComputationPostion), 3);
        var particleSystem = new THREE.ParticleSystem(this.colorComputerGeometry,
                                    this.getColorComputerMaterial(colorBuffer));

        this.colorComputerScene.add(particleSystem);


        // Final pass
        var quadVertices = [
            -1.0, -1.0, 0.0,
            1.0, -1.0, 0.0,
            -1.0, 1.0, 0.0,
            1.0, 1.0, 0.0,
        ];

        var quadIndices = [
            0, 1, 2,
            1, 3, 2
        ];
        var uv = [
            0.0, 0.0,
            1.0, 0.0,
            0.0, 1.0,
            1.0, 1.0
        ]
        var geom = new THREE.BufferGeometry();

        geom.addAttribute("position", new Float32Array(quadVertices), 3);
        geom.addAttribute("index", new Uint16Array(quadIndices), 1);
        geom.addAttribute("uv", new Float32Array(uv), 2);

        this.finalPassGeometry = geom;

        this.finalPassMaterial = this.getFinalPassMaterial();
        this.finalPassMesh = new THREE.Mesh(this.finalPassGeometry, this.finalPassMaterial);
        this.finalPassScene.add(this.finalPassMesh);
    }


    public update(renderer: THREE.WebGLRenderer, camera: THREE.Camera) {
        renderer.setRenderTarget(this.colorComputerTexture);
        renderer.clear();
        renderer.render(this.colorComputerScene, camera, this.colorComputerTexture);

        renderer.setRenderTarget(this.finalPassRenderTarget);
        renderer.clear();
    //   renderer.render(this.finalPassScene, camera);// this.finalPassRenderTarget);
        renderer.render(this.finalPassScene, camera, this.finalPassRenderTarget);
        renderer.setRenderTarget(null);
    }



}