﻿/// <reference path="./lib/require.d.ts" />
/// <reference path="./lib/jquery.d.ts" />
/// <reference path="./lib/three.d.ts" />

import Background = require("background");
import PostProcessing = require("post-processing");
import FramebufferReader = require("framebuffer-reader");


declare var Stats;

function initCamera() {
    var camera = new THREE.OrthographicCamera(80, 1.0, 0.1, 1000);
    camera.position.y = 1;
    camera.position.x = 0;
    camera.position.z = 0;
    camera.lookAt(new THREE.Vector3(0, 0, 0));

    return camera;
}

export function initRendering() {
    var canvasWidth = 3200;
    var canvasHeight = 1800;
    var nbElementWidth = 40;
    var nbElementHeight = 40;
    var texture = THREE.ImageUtils.loadTexture("images/fire-rose.jpg");

    var camera = initCamera();
    var offscreenScene = new THREE.Scene();
    var displayedScene = new THREE.Scene();
    var renderer = new THREE.WebGLRenderer();
    document.body.appendChild(renderer.domElement);

    var windowWidth = window.innerWidth - 128;
    renderer.setSize(windowWidth, windowWidth * canvasHeight / canvasWidth);
    renderer.setClearColor(new THREE.Color(0.0, 0.0, 0.0), 0.0);
    renderer.clear();
    renderer.autoClear = false;
    renderer.setFaceCulling(THREE.CullFaceNone);


    var background = Background.initBackground(texture)
    offscreenScene.add(background);

    var triangulizer = new PostProcessing.TriangularizerRenderer(renderer, canvasWidth, canvasHeight, nbElementWidth, nbElementHeight);

    function render() {
        window.requestAnimationFrame(render);

        renderer.setRenderTarget(null);
        renderer.clear();
        triangulizer.render(renderer, offscreenScene, camera, null);
    }

    render();
}

// we assume the image is ready
export function renderImage(image: HTMLImageElement, patternWidth: number, patternHeight: number, targetWidth: number = -1, targetHeight: number = -1): HTMLImageElement {
    var width = targetWidth == -1 ? image.width : targetWidth;
    var height = targetHeight == -1 ? image.height: targetHeight;

    var texture = new THREE.Texture(image);
    texture.needsUpdate = true;

    var camera = initCamera();
    var scene = new THREE.Scene();
    var renderer = new THREE.WebGLRenderer({ preserveDrawingBuffer: true });

    renderer.setSize(width, height);
    renderer.setClearColor(new THREE.Color(0.0, 0.0, 0.0), 0.0);
    renderer.clear();
    renderer.autoClear = false;
    renderer.setFaceCulling(THREE.CullFaceNone);

    var triangulizer = new PostProcessing.TriangularizerRenderer(renderer, width, height, patternWidth, patternHeight);

    var target = new THREE.WebGLRenderTarget(width, height,
        {
            minFilter: THREE.LinearFilter, magFilter: THREE.LinearFilter,
            format: THREE.RGBAFormat
        });

    target = null;

    var background = Background.initBackground(texture)
    scene.add(background);

    renderer.setRenderTarget(target);
    renderer.clear();
    triangulizer.render(renderer, scene, camera, target);

    var image = new Image();
    image.src = renderer.domElement.toDataURL();
    image.className = "triangulized-img";

    return image;
}
