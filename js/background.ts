﻿export function initBackground(texture: THREE.Texture = null) {
    texture = texture || THREE.ImageUtils.loadTexture("images/blue-flower.jpg");

    var vertShader = document.getElementById('background-vertex').innerHTML;
    var fragShader = document.getElementById('background-fragment').innerHTML;

    var uniforms = {
        tex: { type: "t", value: texture }
    };

    var material = new THREE.ShaderMaterial({
        uniforms: uniforms,
        vertexShader: vertShader,
        fragmentShader: fragShader
    });

    var geom = new THREE.Geometry();

    geom.vertices.push(new THREE.Vector3(-1, -1, 0),
        new THREE.Vector3(-1, 1, 0),
        new THREE.Vector3(1, -1, 0),
        new THREE.Vector3(1, 1, 0));

    geom.faces.push(new THREE.Face3(0, 2, 1));
    geom.faces.push(new THREE.Face3(1, 2, 3));

    var quad = new THREE.Mesh(geom, material);


    return quad;
}


var centerPosition = new THREE.Vector2(0.0, 0.0);
var radius = 0;
var detailsMaterial 



export function updateDetailsPattern() {
   // if (radius > 6) {
        centerPosition.x = 0.0;
        centerPosition.y = -0.1;
        centerPosition.normalize();
        detailsMaterial.uniforms.centerPosition.needsUpdate = true;
        radius = 1.0;
        detailsMaterial.uniforms.delta0.value = 0.1;
        detailsMaterial.uniforms.delta1.value = 2;
        detailsMaterial.uniforms.delta2.value = 2;
//    }

 //   radius += 0.02;
    detailsMaterial.uniforms.radius.value = radius;
    detailsMaterial.uniforms.radius.needsUpdate = true;

  //  detailsMaterial.uniforms.delta0.value =  0.4 + radius / 4;
  //  detailsMaterial.uniforms.delta1.value = 0.8 + radius / 4;
  //  detailsMaterial.uniforms.delta1.value = 1.6 + radius / 4;

//    detailsMaterial.uniforms.delta0.needsUpdate = true;
//    detailsMaterial.uniforms.delta1.needsUpdate = true;
//    detailsMaterial.uniforms.delta2.needsUpdate = true;
}

export function initDetailsPattern(lvl0: THREE.Texture,
    lvl1: THREE.Texture,
    lvl2: THREE.Texture,
    lvl3: THREE.Texture) {

    var vertShader = document.getElementById('tesselation-picker-vertex').innerHTML;
    var fragShader = document.getElementById('tesselation-picker-fragment').innerHTML;



    var uniforms = {
        tex0: { type: "t", value: lvl0 },
        tex1: { type: "t", value: lvl1 },
        tex2: { type: "t", value: lvl2 },
        tex3: { type: "t", value: lvl3 },

        delta0: { type: "f", value: 0.2 },
        delta1: { type: "f", value: 0.4 },
        delta2: { type: "f", value: 0.8 },

        centerPosition: { type: "v2", value: centerPosition },
        radius: { type: "f", value: radius }
    };

    detailsMaterial = new THREE.ShaderMaterial({
        uniforms: uniforms,
        vertexShader: vertShader,
        fragmentShader: fragShader
    });

    var geom = new THREE.Geometry();

    geom.vertices.push(new THREE.Vector3(-1, -1, 0),
        new THREE.Vector3(-1, 1, 0),
        new THREE.Vector3(1, -1, 0),
        new THREE.Vector3(1, 1, 0));

    geom.faces.push(new THREE.Face3(0, 2, 1));
    geom.faces.push(new THREE.Face3(1, 2, 3));

    var quad = new THREE.Mesh(geom, detailsMaterial);


    return quad;
}