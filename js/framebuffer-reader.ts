﻿export function getImageDataFromFramebuffer(renderer: THREE.WebGLRenderer, fb: THREE.RenderTarget, x: number, y: number, width: number, height: number): Uint8Array {
    var context = renderer.getContext();
    var buf = new Uint8Array(width * height * 4); // assuming RGBA

    renderer.setRenderTarget(fb);

    context.readPixels(x, y, width, height, context.RGBA, context.UNSIGNED_BYTE, buf);

    return buf;
} 